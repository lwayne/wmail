#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import poplib, sys, email, os, shutil
import smtplib, mimetypes
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

# 屈居变量
popserver = ''
sender = ''
receiver = ''
smtpserver = ''
username = ''
password = ''

class sendMail:
    '''
    邮件发送类
    '''
    subject = 'test'
    content = 'wayne am i'
    
    def __init__(self, smtpserver, username, password):
        self.smtpserver = smtpserver
        self.username = username
        self.password = password
    
    def sendAMail(self, receiver, subject, sender, content, filename=None):  
        '''
        发送电子邮件
        '''
        self.receiver = receiver
        self.subject = subject
        self.content = content
        self.sender = sender
        print self.sender
        
        try:  
            message = MIMEMultipart()  
            message.attach(MIMEText(self.content))  
            message["Subject"] = self.subject  
            message["From"] = self.sender  
            message["To"] = self.receiver      #";".join(self.receiver)  
                                                            
            if filename != None and os.path.exists(filename):  
                attachment = MIMEText(open(filename, 'rb').read(), 'base64', 'utf-8')
                attachment["Content-Type"] = 'application/octet-stream'
                attachment["Content-Disposition"] = 'attachment; filename=' + filename
                message.attach(attachment)
#                 ctype, encoding = mimetypes.guess_type(filename)  
#                 if ctype is None or encoding is not None:  
#                     ctype = "application/octet-stream" 
#                 maintype, subtype = ctype.split("/", 1)  
#                 attachment = MIMEImage((lambda f: (f.read(), f.close()))(open(filename, "rb"))[0], _subtype = subtype)  
#                 attachment.add_header("Content-Disposition", "attachment", filename = filename)  
#                 message.attach(attachment)  
#  
            smtp = smtplib.SMTP()  
            smtp.connect(self.smtpserver)  
            smtp.login(self.username, self.password)  
            smtp.sendmail(self.sender, self.receiver, message.as_string())  
            smtp.quit()  
            return True 
        
        except Exception, errmsg:  
            print "发送失败: %s" % errmsg  
            return False 
        
class receiveMail:
    '''
    邮件接收类

    '''
    
    def __init__(self, popserver, username, password):
        self.popserver = popserver
        self.username = username
        self.password = password
        
    def Download(self):
        '''
        下载邮件到文件
        '''
        p = poplib.POP3(self.popserver)
        try:
            p.user(self.username)
            p.pass_(self.password)
        except poplib.error_proto, e:
            print "Login failed:", e
            sys.exit(1)

    # 删除之前的邮件
        mailfile = os.listdir('mail')
        for mail in mailfile:
            os.remove('mail\\%s' % mail) 

 
        for item in p.list()[1]:
            number, octets = item.split(' ')

#            print number,octets
  
            destfd = open('mail\\%s.eml' % str(number), 'at')
         
#            print "Downloading message %s (%s bytes)" % (number, octets)

            lines = p.retr(number)[1]
            message = email.message_from_string("\n".join(lines))
            destfd.write(message.as_string(unixfrom=1))

#         Make sure there's an extra newline separating messages
            destfd.write("\n")
            destfd.close()
        
        p.quit()
  
class Mail :
    '''
    Mail类，提取header

    '''
    
    file = ' '
    
    def __init__(self, file): 
        self.file = file
        
    def getSubject(self):
        '''
        获取邮件主题
        '''
        fp = open('mail\\%s.eml' % file, "r")  # 打开文件
        msg = email.message_from_file(fp)
        fp.close()
        
        subTupleList = email.Header.decode_header(msg.get('subject'))  # 解码头
        subject, encode = subTupleList[0]  #
        if subcode is None:
            return subject
        
        else :
            unicode(subject, subcode)
            return subject
        
         
    
    
    
    
    
    
    
    
            
