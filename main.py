# -*- encoding: utf-8 -*-
#!/usr/bin/env  python
import wx
import wx.html
import wx.calendar
import wx.lib.filebrowsebutton
import wx.lib.analogclock
from functions import *
from eMail import *
from contact import *
from agenda import *

class MTB(wx.Toolbook):
    '''
    建立toolbook，parent 为 主框架
    '''
    def __init__(self, parent, id):
        wx.Toolbook.__init__(self, parent, id, size=(800, 680), style=  # 注意：size一定要设置
                             wx.BK_DEFAULT
                            )
        il = wx.ImageList(48, 48)  # 跟图像的大小一致
        bmp0 = wx.Image('icon\\0.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        bmp1 = wx.Image('icon\\1.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        bmp2 = wx.Image('icon\\2.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        bmp3 = wx.Image('icon\\3.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        self.index0 = il.Add(bmp0)
        self.index1 = il.Add(bmp1)
        self.index2 = il.Add(bmp2)
        self.index3 = il.Add(bmp3)
        self.AssignImageList(il)
        self.createWin1()
        self.createWin2()
        self.createWin3()
        self.createWin4()
 
    
    def createWin1(self):      
        '''
        首页页面
        包括，账号设置，日程管理
        '''      
        #首先获取账号设置
        global username, password, sender, popserver, smtpserver
        c = account()
        acc = c.getAccount()
        username = acc[1]
        password = acc[2]
        sender = acc[3]
        popserver = acc[4]
        smtpserver = acc[5]
        
        self.win1 = wx.Panel(self, 1)
        self.win1.SetBackgroundColour(wx.Colour(215, 234, 220))
        self.AddPage(self.win1, u"    首    页    ", imageId=self.index0)
        self.win1.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.NORMAL, False, u'微软雅黑'))
        
        
        self.staticTextWelcome = wx.StaticText(id=-1,
              label=u'欢迎使用WMail', name='staticTextWelcome',
              parent=self.win1, pos=wx.Point(112, 24), size=wx.Size(289, 46),
              style=0)
        self.staticTextWelcome.SetFont(wx.Font(26, wx.SWISS, wx.NORMAL,
              wx.NORMAL, False, u'微软雅黑'))
        self.staticTextWelcome.SetForegroundColour(wx.Colour(187, 190, 44))

        self.staticTextInfo = wx.StaticText(id= -1,
              label=u'先配置再使用', name='staticTextInfo', parent=self.win1,
              pos=wx.Point(232, 96), size=wx.Size(145, 28), style=0)

        self.staticBoxAcc = wx.StaticBox(id=-1,
              label=u'账号配置', name='staticBoxAcc', parent=self.win1,
              pos=wx.Point(24, 152), size=wx.Size(288, 360), style=0)
        self.staticBoxAcc.SetForegroundColour(wx.Colour(245, 61, 107))

        self.staticBoxAgenda = wx.StaticBox(id=-1,
              label=u'日程备忘', name='staticBoxAgenda', parent=self.win1,
              pos=wx.Point(504, 152), size=wx.Size(264, 360), style=0)
        self.staticBoxAgenda.SetForegroundColour(wx.Colour(19, 19, 119))

        self.staticTextMUser = wx.StaticText(id=-1,
              label=u'用户名：', name='staticTextMUser', parent=self.win1,
              pos=wx.Point(32, 200), size=wx.Size(97, 21), style=0)
        self.staticTextMUser.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.NORMAL,
              False, u'微软雅黑'))

        self.staticTextMPwd = wx.StaticText(id=-1,
              label=u'密码:', name='staticTextMPwd', parent=self.win1,
              pos=wx.Point(32, 248), size=wx.Size(43, 21), style=0)

        self.staticTextMSender = wx.StaticText(id=-1,
              label=u'邮箱：', name='staticTextMSender', parent=self.win1,
              pos=wx.Point(32, 296), size=wx.Size(69, 21), style=0)

        self.staticTextMPop = wx.StaticText(id=-1,
              label=u'收件服务器:', name='staticTextMPop', parent=self.win1,
              pos=wx.Point(32, 344), size=wx.Size(96, 21), style=0)

        self.staticTextMSmtp = wx.StaticText(id=-1,
              label=u'发件服务器:', name='staticTextMSmtp', parent=self.win1,
              pos=wx.Point(32, 392), size=wx.Size(64, 21), style=0)

        self.textCtrlMUser = wx.TextCtrl(id=-1,
              name='textCtrlMUser', parent=self.win1, pos=wx.Point(116, 200),
              size=wx.Size(150, 29), style=0, value=username)

        self.textCtrlMPwd = wx.TextCtrl(id=-1,
              name='textCtrlMPwd', parent=self.win1, pos=wx.Point(116, 248),
              size=wx.Size(150, 29), style=wx.TE_PASSWORD, value=password)

        self.textCtrlMSender = wx.TextCtrl(id=-1,
              name='textCtrlMSender', parent=self.win1, pos=wx.Point(116, 296),
              size=wx.Size(150, 29), style=0, value=sender)

        self.textCtrlMPop = wx.TextCtrl(id=-1,
              name='textCtrlMPop', parent=self.win1, pos=wx.Point(116, 344),
              size=wx.Size(150, 29), style=0, value= popserver)

        self.textCtrlMSmtp = wx.TextCtrl(id=-1,
              name='textCtrlMSmtp', parent=self.win1, pos=wx.Point(116, 392),
              size=wx.Size(150, 29), style=0, value=smtpserver)

        self.buttonMAcc = wx.Button(id=-1,
              label=u'提交', name='buttonMAcc', parent=self.win1,
              pos=wx.Point(96, 448), size=wx.Size(88, 31), style=0)
        self.buttonMAcc.Bind(wx.EVT_BUTTON, self.OnButtonMAccButton, id=-1)

        self.textCtrlACon = wx.TextCtrl(id=-1,
              name='textCtrlACon', parent=self.win1, pos=wx.Point(536, 272),
              size=wx.Size(224, 144), style=wx.TE_MULTILINE, value=' ')

        self.staticTextATime = wx.StaticText(id=-1,
              label=u'日期：', name='staticTextATime', parent=self.win1,
              pos=wx.Point(512, 203), size=wx.Size(48, 21), style=0)

        self.datePickerCtrlA = wx.DatePickerCtrl(id=-1,
              name='datePickerCtrlA', parent=self.win1, pos=wx.Point(580, 200),
              size=wx.Size(161, 29), style=wx.DP_SHOWCENTURY)

        self.staticTextACon = wx.StaticText(id=-1,
              label=u'内容：', name='staticTextACon', parent=self.win1,
              pos=wx.Point(512, 248), size=wx.Size(73, 21), style=0)

        self.buttonAAga = wx.Button(id=-1,
              label=u'提交', name='buttonAAga', parent=self.win1,
              pos=wx.Point(584, 448), size=wx.Size(88, 31), style=0)
        self.buttonAAga.Bind(wx.EVT_BUTTON, self.OnButtonAAgaButton, id=-1)
        
       
    def createWin2(self):
        '''
        收件箱页面
        '''
        self.win2 = wx.Panel(self, 2)
        self.win2.SetBackgroundColour(wx.Colour(227, 239, 244))
        self.AddPage(self.win2, u"    收件箱    ", imageId=self.index1)
        self.listBoxMsg = wx.ListBox(choices=[], id= -1,
              name='listBoxMsg', parent=self.win2, pos=wx.Point(5, 40),
              size=wx.Size(240, 430), style=0)
        self.getEMailList()
        
        self.staticTextSender = wx.StaticText(id= -1,
              label=u'发件人:', name='staticTextSender', parent=self.win2,
              pos=wx.Point(260, 40), size=wx.Size(62, 14), style=0)

        self.textCtrlSender = wx.TextCtrl(id= -1, name='textCtrlSender',
              parent=self.win2, pos=wx.Point(340, 40), size=wx.Size(200, 22),
              style=0, value=' ')
        self.mailHtml = wx.html.HtmlWindow(parent=self.win2, id= -1, pos=wx.Point(246, 70),
                                      size=wx.Size(550, 400),
                                      style=wx.html.HW_SCROLLBAR_AUTO | wx.DOUBLE_BORDER)

        self.buttonReceive = wx.Button(id= -1, label=u'收信',
              name='buttonReceive', parent=self.win2, pos=wx.Point(240, 10),
              size=wx.Size(160, 24), style=0)
        self.buttonReceive.Bind(wx.EVT_BUTTON, self.DownLoad, id= -1)
        comboBox1 = wx.ComboBox(choices=[], id= -1,
              name='comboBox1', parent=self.win2, pos=wx.Point(6, 10),
              size=wx.Size(190, 22), style=0, value='Nome')
        return self.win2
        
    def createWin3(self):     
        '''
        写信页面
        '''
        self.win3 = wx.Panel(self, 3)
        self.win3.SetBackgroundColour(wx.Colour(245, 236, 231))
        self.AddPage(self.win3, u"    写信    ", imageId=self.index2)
        
        self.staticTextReceiver = wx.StaticText(id= -1,
              label=u'收件人', name='staticTextReceiver', parent=self.win3,
              pos=wx.Point(208, 32), size=wx.Size(64, 24), style=0)

        self.textCtrlReceiver = wx.TextCtrl(id= -1,
              name='textCtrlReceiver', parent=self.win3, pos=wx.Point(280,
              32), size=wx.Size(408, 22), style=0, value=' ')

        self.staticTextSubject = wx.StaticText(id= -1,
              label=u'主题', name='staticTextSubject', parent=self.win3,
              pos=wx.Point(208, 64), size=wx.Size(40, 16), style=0)

        self.textCtrlSubject = wx.TextCtrl(id= -1,
              name='textCtrlSubject', parent=self.win3, pos=wx.Point(280, 64),
              size=wx.Size(408, 22), style=0, value=' ')

        self.fileBrowseButton1 = wx.lib.filebrowsebutton.FileBrowseButton(buttonText=u'浏览',
              dialogTitle=u'选择一个文件', fileMask='*.*',
              id= -1, initialValue=' ',
              labelText=u'附件：', parent=self.win3, pos=wx.Point(216, 88),
              size=wx.Size(480, 48), startDirectory='.', style=wx.TAB_TRAVERSAL,
              toolTip=u'输入文件名，或者选择一个文件')

        self.staticTextCotent = wx.StaticText(id= -1,
              label=u'正文', name='staticTextCotent', parent=self.win3,
              pos=wx.Point(208, 144), size=wx.Size(57, 14), style=0)

        self.textCtrlCotent = wx.TextCtrl(id= -1, name='textCtrlCotent',
              parent=self.win3, pos=wx.Point(272, 144), size=wx.Size(416,
              288), style=wx.TE_MULTILINE, value='')

        self.buttonSend = wx.Button(id= -1, label=u'发送', name='buttonSend',
              parent=self.win3, pos=wx.Point(272, 456), size=wx.Size(75, 24),
              style=0)
        self.buttonSend.Bind(wx.EVT_BUTTON, self.OnButtonSend, id= -1)
        
        self.listBoxContact = wx.ListBox(choices=[], id= -1,
              name='listBoxContact', parent=self.win3, pos=wx.Point(24, 64),
              size=wx.Size(160, 368), style=0)
        self.getContactList()

        self.staticTextContact = wx.StaticText(id= -1,
              label=u'联系人', name='staticTextContact', parent=self.win3,
              pos=wx.Point(32, 40), size=wx.Size(43, 14), style=0)
        
        
    def createWin4(self):     
        '''
        win4为联系人页面
        MC = maneage contact
        '''
        self.win4 = wx.Panel(self, 4)
        self.win4.SetBackgroundColour(wx.Colour(194, 252, 198))
        self.AddPage(self.win4, u"    联系人    ", imageId=self.index3)
        
        self.listBoxMC = wx.ListBox(choices=[], id= -1,
              name='listBoxMC', parent=self.win4, pos=wx.Point(40, 72),
              size=wx.Size(176, 368), style=0)
        self.getMCList()

        self.staticTextMC = wx.StaticText(id= -1,
              label=u'本地联系人', name='staticTextMC', parent=self.win4,
              pos=wx.Point(48, 40), size=wx.Size(73, 14), style=0)

        self.staticTextMCName = wx.StaticText(id= -1,
              label=u'姓名：', name='staticTextMCName', parent=self.win4,
              pos=wx.Point(280, 72), size=wx.Size(48, 14), style=0)

        self.staticTextMCAdd = wx.StaticText(id= -1,
              label=u'邮箱地址', name='staticTextMCAdd', parent=self.win4,
              pos=wx.Point(280, 136), size=wx.Size(61, 14), style=0)

        self.staticTextMCTel = wx.StaticText(id= -1,
              label=u'电话：', name='staticTextMCTel', parent=self.win4,
              pos=wx.Point(280, 200), size=wx.Size(42, 14), style=0)

        self.textCtrlMCName = wx.TextCtrl(id= -1, name='textCtrlMCName',
              parent=self.win4, pos=wx.Point(344, 96), size=wx.Size(200, 22),
              style=0, value=' ')

        self.textCtrlMCAdd = wx.TextCtrl(id= -1, name='textCtrlMCAdd',
              parent=self.win4, pos=wx.Point(344, 160), size=wx.Size(200, 22),
              style=0, value=' ')

        self.textCtrlMCTel = wx.TextCtrl(id= -1, name='textCtrlMCTel',
              parent=self.win4, pos=wx.Point(344, 224), size=wx.Size(200, 22),
              style=0, value=' ')

        self.buttonMCAdd = wx.Button(id= -1, label=u'添加',
              name='buttonMCAdd', parent=self.win4, pos=wx.Point(288, 296),
              size=wx.Size(75, 24), style=0)
        self.buttonMCAdd.Bind(wx.EVT_BUTTON, self.OnButtonMCAdd, id= -1)

        self.buttonMCDel = wx.Button(id= -1, label=u'删除',
              name='buttonMCDel', parent=self.win4, pos=wx.Point(288, 344),
              size=wx.Size(75, 24), style=0)
        self.buttonMCDel.Bind(wx.EVT_BUTTON, self.OnButtonMCDel, id= -1)

        self.buttonMCEdi = wx.Button(id= -1, label=u'修改',
              name='buttonMCEdi', parent=self.win4, pos=wx.Point(288, 392),
              size=wx.Size(75, 24), style=0)
        self.buttonMCEdi.Bind(wx.EVT_BUTTON, self.OnButtonMCEdi, id= -1)
        
        
        
        
    def getEMailList(self):
        '''
        获取邮件列表
        '''
        files = readFileNames('mail')
        files.sort(reverse=False)
        # 或得文件中的邮件信息
        for file in files:
            fp = open(file, "r")
            msg = email.message_from_file(fp)
            fp.close()
            subTupleList = email.Header.decode_header(msg.get('subject'))
            subject, subcode = subTupleList[0]
            
            if subcode:
                self.listBoxMsg.Append(unicode(subject, subcode), file)
            else:
                try:
                    self.listBoxMsg.Append(subject)
                except:
                    print subject, subcode
        self.listBoxMsg.Bind(wx.EVT_LISTBOX, self.OnListBoxMsgSel)
        
    def OnListBoxMsgSel(self, event):
        '''
        收件箱 列表框 事件函数
        '''
        file = event.GetClientData()
        
        data = Mime(file)
        rdmail = data[1]
        try:
            self.mailHtml.SetPage(str(' '))
            self.textCtrlSender.SetValue(rdmail[1])
            self.mailHtml.SetPage(rdmail[4])
        except:
            pass
        event.Skip()
        
    def DownLoad(self, event):
        '''
        邮件下载按钮的响应函数
        '''
            #进度条
        dialog  = wx.ProgressDialog("A progress box", "Time remaining", 100,
                                    style=wx.PD_ELAPSED_TIME | wx.PD_REMAINING_TIME)
        
        #变量
        global popserver, username, password
        down = receiveMail(popserver, username, password)
        dialog.Update(40)
        
        down.Download()
        dialog.Update(98)
        self.listBoxMsg.Clear()
        self.getEMailList()
        dialog.Update(100)
        dialog.Destroy()
        
    def OnButtonSend(self, event):
        '''
        邮件发送按钮的响应函数
        '''
        global smtpserver, username, password, receiver, sender
        receiver = self.textCtrlReceiver.GetValue()
        subject = self.textCtrlSubject.GetValue()
        content = self.textCtrlCotent.GetValue()
        sender = self.textCtrlMSender.GetValue()
        att = self.fileBrowseButton1.GetValue()
        if ' ' != att:
            mail = sendMail(smtpserver, username, password)
            sta = mail.sendAMail(receiver, subject, sender, content, att)
        else:
            mail = sendMail(smtpserver, username, password)
            sta = mail.sendAMail(receiver, subject, sender, content)
        if sta:
            dlg = wx.MessageDialog(None, "发送成功！",
                                'Message Box',wx.OK | wx.ICON_QUESTION)
            retCode = dlg.ShowModal()
            if (retCode == wx.ID_OK):
                print 'succ'
            else:
                print 'fail'
            dlg.Destroy() 
            
    def getContactList(self):
        '''
        获取联系人添加到listbox中
        '''
        c = contact()
        cList = c.getAllContacter()
        for contacter in cList:
            try:
                self.listBoxContact.Append(contacter[1], contacter)
            except:
                pass
        self.listBoxContact.Bind(wx.EVT_LISTBOX, self.OnListBoxConSel)

    def OnListBoxConSel(self,event): 
        '''
        发送界面联系人选择
        '''
        receiver = event.GetClientData()
        self.textCtrlReceiver.SetValue(receiver[2])
        event.Skip()
        
    def getMCList(self):
        '''
        获取联系人添加到listbox中
        '''
        c = contact()
        cList = c.getAllContacter()
        for contacter in cList:
            try:
                self.listBoxMC.Append(contacter[1], contacter)
            except:
                pass
        self.listBoxMC.Bind(wx.EVT_LISTBOX, self.OnListBoxMCSel, id= -1)

    def OnListBoxMCSel(self, event):
        self.cM = event.GetClientData()
        self.textCtrlMCName.SetValue(self.cM[1])
        self.textCtrlMCAdd.SetValue(self.cM[2])
        self.textCtrlMCTel.SetValue(self.cM[3])
        event.Skip()
        
    def OnButtonMCAdd(self, event):
        '''
        添加联系人按钮事件
        '''
        c = contact()
        name = self.textCtrlMCName.GetValue()
        email = self.textCtrlMCAdd.GetValue()
        tel = self.textCtrlMCTel.GetValue()
        c.addContacter(name, email, tel)
        self.listBoxMC.Clear()
        self.getMCList()
        event.Skip()

    def OnButtonMCDel(self, event):
        '''
        联系人管理——删除
        '''
        c = contact()
        cid = self.cM[0]
        c.delContacter(cid)
        self.listBoxMC.Clear()
        self.getMCList()
        event.Skip()

    def OnButtonMCEdi(self, event):
        '''
        联系人管理------修改
        '''
        c = contact()
        cid = self.cM[0]
        if cid: 
            name = self.textCtrlMCName.GetValue()
            email = self.textCtrlMCAdd.GetValue()
            tel = self.textCtrlMCTel.GetValue()
            c.updateContacter(cid, name, email, tel)
            self.listBoxMC.Clear()
            self.getMCList()
        event.Skip()
        
    def OnButtonMAccButton(self, event):
        '''
        首页帐号管理===提交
        '''
        username = self.textCtrlMUser.GetValue()
        password = self.textCtrlMPwd.GetValue()
        sender = self.textCtrlMSender.GetValue()
        popserver = self.textCtrlMPop.GetValue()
        smtpserver = self.textCtrlMSmtp.GetValue()
        c = account()
        c.updateAccount('0', username, password, sender, popserver, smtpserver)
        event.Skip()

    def OnButtonAAgaButton(self, event):
        '''
        首页---------日程管理===提交
        '''
        date = datetimeToString(self.datePickerCtrlA.GetValue())
        content = self.textCtrlACon.GetValue()
        print date
        a = agenda()
        a.addAgenda(date, content)
        event.Skip()
        
                
                
class WMail(wx.Frame):
    '''
    程序主框架
    '''
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, 'WMail',
        size=(1024, 700))
        panel = wx.Panel(self, -1)
        panel.SetBackgroundColour(wx.Colour(239, 239, 239, 255))
        # TOOLBOOK
        self.tb = MTB(panel, -1)
        self.tb.Bind(wx.EVT_TOOLBOOK_PAGE_CHANGED, self.OnPageChanged)  # 事件
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        self.createMenuBar()  
        self.createCal()

        
        
    def createCal(self):
        '''
        右侧提示信息
        '''
        self.staticBox1 = wx.StaticBox(id= -1,
              label=u'日程', name='staticBox1',
              parent=self, pos=wx.Point(800, 80), size=wx.Size(210, 600),
              style=0)
        self.staticBox1.SetBackgroundColour(wx.Colour(248, 243, 192))
#         cpanel = wx.Panel(id= -1, name='mpanel', parent=self.staticBox1,
#                               pos=wx.Point(0, 0), size=wx.Size(320, 240))
        self.analogClock1 = wx.lib.analogclock.analogclock.AnalogClock(id= -1,
                                                        name='analogClock1', parent=self.staticBox1, pos=wx.Point(5, 24),
                                                       size=wx.Size(200, 100), style=0)
        self.analogClock1.SetBackgroundColour(wx.Colour(255, 255, 255, 100))
        self.calendarCtrl1 = wx.calendar.CalendarCtrl(date=wx.DateTime.Now(),
              id= -1, name='calendarCtrl1',
              parent=self.staticBox1, pos=wx.Point(5, 135), size=wx.Size(200,
              145), style=wx.calendar.CAL_SHOW_HOLIDAYS)
        self.calendarCtrl1.SetAutoLayout(True)
        self.calendarCtrl1.Bind(wx.calendar.EVT_CALENDAR, self.OnCalendarCtrlDclik, id= -1)
        self.listBox1 = wx.ListBox(choices=[], id=-1,
              name='listBox1', parent=self.staticBox1, pos=wx.Point(5, 296),
              size=wx.Size(200, 224), style=0)
        self.agendaList()
        
    def OnCalendarCtrlDclik(self, event):
        print event
        
    def agendaList(self):
        self.listBox1.Clear()
        objectA = agenda()
        listA = objectA.getAllAgenda()
        for a in listA:
            self.listBox1.Append(a[1])
            self.listBox1.Append(a[2])
    
    def createMenuBar(self):  
        '''
        创建菜单
        '''
        menuBar = wx.MenuBar()
        
        menu0 = wx.Menu()  # menu0
        menuItemOpen = menu0.Append(-1, u"&打开", "open")
        self.Bind(wx.EVT_MENU, self.OnOpen, menuItemOpen)
        menuItem = menu0.Append(-1, u"&保存", "open")
        menuItem = menu0.Append(-1, u"&另存为", "open")
        menuItemQuit = menu0.Append(-1, u"&退出", "quit")
        self.Bind(wx.EVT_MENU, self.OnCloseWindow, menuItemQuit)
     
        menu1 = wx.Menu()  # menu1
        menuItemOpen = menu1.Append(-1, u"&拷贝哦", "open")
        self.Bind(wx.EVT_MENU, self.OnOpen, menuItemOpen)
        menuItem = menu1.Append(-1, u"&剪切", "open")
        menuItem = menu1.Append(-1, u"&粘贴", "open")
        menuItemQuit = menu1.Append(-1, u"&查找", "quit")
        self.Bind(wx.EVT_MENU, self.OnCloseWindow, menuItemQuit)
        
        menu2 = wx.Menu()  # menu2
        menuItemOpen = menu2.Append(-1, u"&帐号设置", "open")
        self.Bind(wx.EVT_MENU, self.OnOpen, menuItemOpen)
        menuItem = menu2.Append(-1, u"&联系人设置", "open")
        menuItem = menu2.Append(-1, u"&编辑器设置", "open")
        menuItemQuit = menu2.Append(-1, u"&系统设置", "quit")
        self.Bind(wx.EVT_MENU, self.OnCloseWindow, menuItemQuit)
        
        menu3 = wx.Menu()  # meunu2
        menuItemAbout = menu3.Append(-1, u"&关于", "about")
        self.Bind(wx.EVT_MENU, self.OnAbout, menuItemAbout)
      
        menuBar.Append(menu0, u"&文件")
        menuBar.Append(menu1, u"&编辑")
        menuBar.Append(menu2, u"&设置")
        menuBar.Append(menu3, u"&关于")
      
        self.SetMenuBar(menuBar)
        

    def OnPageChanged(self, event):
        '''
        toolbook 页便切换时间函数
        '''
        old = event.GetOldSelection()
        sel = self.tb.GetSelection()
        self.agendaList()
        if sel == 2:
            self.tb.listBoxContact.Clear()
            self.tb.getContactList()
        
        event.Skip()

    def OnOpen(self, event):
        dlg = wx.MessageDialog(None, "Is this explanation OK",
                                'MessageBox',wx.OK | wx.ICON_QUESTION)
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            print 'yes'
        else:
            print 'no'
        dlg.Destroy()
#         dlg = wx.Dialog(None, id= -1, title=u'消息', style=0, size=(320, 240))
#         mpanel = wx.Panel(id= -1, name='mpanel', parent=dlg,
#                             pos=wx.Point(0, 0), size=wx.Size(320, 240))
#         okButton = wx.Button(mpanel, wx.ID_OK, 'OK', pos=(15, 15))  
#         result = dlg.ShowModal() 
#         if result == wx.ID_OK:
#             print 'succ' 
#         else:
#             print 'failed'
#         dlg.Destroy()  
    def OnCopy(self, event):
        pass
    def OnCut(self, event):  pass
    def OnPaste(self, event):  pass
    def OnOptions(self, event):  pass
    def OnAbout(self, event):
        dlg = wx.MessageDialog(None, "This is WMail ",
                                'About',wx.OK | wx.ICON_QUESTION)
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            print 'yes'
        else:
            print 'no'
        dlg.Destroy()
    def OnCloseWindow(self, event):
        self.Destroy()
        
        
if  __name__ == '__main__':
    app = wx.App()
    frame = WMail(parent=None, id= -1)
    frame.Show()
    app.MainLoop()

