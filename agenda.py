# -*- encoding: utf-8 -*-
#!/usr/bin/env python

import sqlite3

class agenda:
    '''
    日程类
    CREATE TABLE IF NOT EXISTS `agenda` (
   `aid` int(5) NOT NULL,
  `date` varchar(40) NOT NULL,
  `content` varchar(200) NOT NULL,
     PRIMARY KEY (`aid`))
    '''
    def __init__(self):
        pass
    
    def getMaxAid(self):
        '''
        得到当前aid最大值
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("SELECT max(aid) FROM agenda")
        maxAid= c.fetchone()
        conn.close()
        return maxAid[0]
    
    def getAllAgenda(self):
        '''
        遍历所有记录存到表中
       '''
        ContentList = []
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        for content in c.execute("SELECT * FROM agenda"):
            ContentList.append(content)
        conn.close()
        return ContentList
    
    def addAgenda(self, date, content):
        '''
        添加一个记录
        '''
        maxAid = self.getMaxAid()
        aid = maxAid + 1    
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("INSERT INTO `agenda` (`aid`, `date`, `content`) VALUES (?, ?, ?)", \
                  (aid, date, content))
        conn.commit()
        conn.close()
        return True
    
    
    def delAgenda(self, aid):
        '''
        删除一个日程
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("DELETE FROM `agenda` WHERE `aid` = ?", (aid,))
        conn.commit()
        conn.close()
        return True

# a = agenda()
# a.delAgenda(7)
