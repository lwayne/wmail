# -*- coding: UTF-8 -*-
#!/usr/bin/env python

import sqlite3

# conn = sqlite3.connect('WMail.db')
# c = conn.cursor()
# 
# #Create table
# c.execute('''CREATE TABLE IF NOT EXISTS `contacter` (
#   `cid` int(5) NOT NULL,
#   `name` varchar(40) NOT NULL,
#   `email` varchar(40) NOT NULL,
#   `tel` varchar(20) DEFAULT NULL,
#   PRIMARY KEY (`cid`)
# )''')
# 
# # Insert a row of data
# c.execute("INSERT INTO `contacter` (`cid`, `name`, `email`, `tel`) VALUES ('1', 'kld', 'prowayne@163.com', '18888')")
# 
# # Save (commit) the changes
# conn.commit()
# 
# # We can also close the connection if we are done with it.
# # Just be sure any changes have been committed or they will be lost.
# 
# for dd in c.execute("SELECT * FROM contacter"):
# #dd= c.fetchone()
#     print dd
# 
#c = account()
#c.addAccount('prowayne', '', 'prowayne@163.com', 'pop.163.com', 'smtp.163.com')
# conn.close()

class contact:
    '''
    联系人类
    获取联系人，添加 修改联系人
    CREATE TABLE IF NOT EXISTS `contacter` (
   `cid` int(5) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,   `tel` varchar(20) DEFAULT NULL,
     PRIMARY KEY (`cid`))
    '''
    def __init__(self):
        pass
    
    def getMaxCid(self):
        '''
        得到当前cid最大值
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("SELECT max(cid) FROM contacter")
        maxCid= c.fetchone()
        conn.close()
        return maxCid[0]
    
    def getAllContacter(self):
        '''
        遍历所有联系人存到表中
        (0, u'wayne', u'prowayne@163.com', u'18888')
        '''
        ContacterList = []
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        for contacter in c.execute("SELECT * FROM contacter"):
            ContacterList.append(contacter)
        conn.close()
        return ContacterList
    
    def addContacter(self, name, email, tel):
        '''
        添加一个联系人
        '''
        maxCid = self.getMaxCid()
        cid = maxCid + 1
        
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("INSERT INTO `contacter` (`cid`, `name`, `email`, `tel`) VALUES (?, ?, ?, ?)", \
                  (cid, name, email, tel))

        conn.commit()
        conn.close()
        return True
    
    def updateContacter(self, cid, name, email, tel):
        '''
        修改联系人
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("UPDATE `contacter` SET `name` = ?, `email` = ?, `tel` = ? WHERE `cid` = ?", \
                  (name, email, tel, cid))

        conn.commit()
        conn.close()
        return True
    
    def delContacter(self, cid):
        '''
        删除一个联系人
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("DELETE FROM `contacter` WHERE `cid` = ?", (cid,))

        conn.commit()
        conn.close()
        return True

class account():
    '''
    账号管理类
    CREATE TABLE IF NOT EXISTS `account` (
   `uid` int(5) NOT NULL,
    `username` varchar(40) NOT NULL,
     `password` varchar(40) NOT NULL,   
    `sender` varchar(40) DEFAULT NULL,
    `popserver` varchar(40) DEFAULT NULL,
    `smtpserver` varchar(40) DEFAULT NULL,

     PRIMARY KEY (`uid`))
     
    '''
    
    def __init__(self):
        pass
    
    def getMaxUid(self):
        '''
        得到当前uid最大值
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("SELECT max(uid) FROM account")
        maxUid= c.fetchone()
        conn.close()
        return maxUid[0]
    
    def getAccount(self):
        '''
        遍历帐户得到一个存到元组中
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("SELECT * FROM `account`")
        account = c.fetchone()
        conn.close()
        return account
    
    def addAccount(self, username, password, sender, popserver, smtpserver):
        '''
        添加一个转户
        '''
        maxUid = self.getMaxUid()
        uid = maxUid + 1
        
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("INSERT INTO `account` (`uid`, `username`, `password`, \
                    `sender`, `popserver`, `smtpserver`) VALUES (?, ?, ?, ?, ?, ?)", \
                    (uid, username, password, sender, popserver, smtpserver))

        conn.commit()
        conn.close()
        return True
    
    def updateAccount(self, uid, username, password, sender, popserver, smtpserver):
        '''
        修改帐户
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("UPDATE `account` SET `username` = ?, `password` = ?, `sender` = ?, `popserver` = ?, `smtpserver` = ? WHERE uid = ?", \
                  (username, password, sender, popserver, smtpserver, uid))

        conn.commit()
        conn.close()
        return True
    
    def delAccount(self, uid):
        '''
        删除一个庄户
        '''
        conn = sqlite3.connect('WMail.db')
        c = conn.cursor()
        c.execute("DELETE FROM `account` WHERE `cid` = ?", uid)

        conn.commit()
        conn.close()
        return True
