# -*- encoding: utf-8 -*-
import os.path
import email, sys, time
from email import Utils
from datetime import datetime
def readFileNames(dir):
    '''
    read all the eml files
    得到一个文件列表
    '''
    fileList = []
    for parent, dirNames, fileNames in os.walk(dir):
        if fileNames:
            for fileName in fileNames:
                fileList.append(os.path.join(parent, fileName))
    
    return fileList


def Mime(mail):
    '''
    read the mail
    获得内容
    '''
    fp = open(mail, "r")
    msg = email.message_from_file(fp)
    fp.close()

    attPaths = []
    message = []

#     subTupleList = email.Header.decode_header(msg.get('subject'))
#     subject,subcode = subTupleList[0]
    hSubject = email.Header.Header(msg.get('subject'))
    dhSubject = email.Header.decode_header(hSubject)
    subject = dhSubject[0][0]
    print hSubject
    print dhSubject
    print subject
    
    mfrom = email.Utils.parseaddr(msg.get('from'))[1]
    mto = email.Utils.parseaddr(msg.get('to'))[1]
    mdate = time.strftime('%A, %B %d %Y at %I:%M %p', \
                             time.localtime(Utils.mktime_tz(Utils.parsedate_tz(msg.get('date')))))
#     if subcode:
#         subject = unicode(subject,subcode)
#         mdate = unicode(mdate,subcode)
#     else:
#         try:
#             subject = str(subject)
#         except:
#             print subject,subcode
    


    
    message = [subject, mfrom, mto, mdate]
       
    for part in msg.walk():
        if not part.is_multipart():
            name = part.get_param("name")  # 判断是否有附件
            if name:
                print "This is attachment"
                h = email.Header.Header(name)
                dh = email.Header.decode_header(h)
                fname = dh[0][0]
                print u"附件名 :",fname
                data = part.get_payload(decode=True)

                try:
                    f = open('att\\' + fname, "wb")  # 注意一定要用wb来打开文件，因为附件一般都是二进制文件
                except:
#                    print "附件名有非法字符，自动换一个"
                    fname = 'attachefile'
                    f = open('att\\' + fname, 'wb')
                f.write(data)
                attPaths.append(fname)
                f.close()
            else:
                message.append(str(part.get_payload(decode=True)))

    return [attPaths, message]

def datetimeToString(dt):
    date = str(dt)
    return date[0:8]
    #return dt.strftime("%Y-%m-%d")
